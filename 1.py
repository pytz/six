from requests.exceptions import RequestException
from lxml import etree
from bs4 import BeautifulSoup
from pyquery import PyQuery as pq
import requests
import re,time,json

def getPage(url):
	try:
		headers = {
			'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'
		}
		res = requests.get(url,headers=headers)
		if res.status_code == 200:
			return res.text
		else:
			return None
	except RequestException:
		return None

def parsePage(content):
	# =======使用xpath解析====================
    # 解析HTML文档，返回根节点对象
    '''
    html = etree.HTML(content)
    #获取网页中所有标签并遍历输出标签名
    items = html.xpath('//table[@width="100%"]')
    #遍历封装数据并返回
    for item in items:
        yield {
            'image':item.xpath('.//img[@width="90"]/@src')[0],
            'title':item.xpath('.//a[@title]/text()')[0].replace(' ','').replace("\n",''),
            'author':item.xpath('.//p[@class="pl"]/text()')[0],
            'score':item.xpath('.//span[@class="rating_nums"]/text()'),
        }
    '''
    # =======使用Beautiful Soup解析====================
    '''
    soup = BeautifulSoup(content,"lxml")
    #获取网页中所有标签并遍历输出标签名
    items = soup.find_all(name="table",attrs={"width":"100%"})
    print(items)
    #遍历封装数据并返回
    for item in items:
        yield {

            'image':item.find(name="img",attrs={'width':'90'}).attrs['src'],
            'title':item.select("div.pl2 a")[0].get_text().replace(' ','').replace("\n",''),
            'author':item.find(name="p",attrs={'class':'pl'}).string, #内有标签使用string获取不到
            'score':item.select("div.star.clearfix span.rating_nums")[0].get_text(),
        } 
    '''
    # =========使用pyquery解析==================
    # 解析HTML文档
    doc = pq(content)
    #获取网页中所有标签并遍历输出标签名
    items = doc("table[width='100%']")
    #遍历封装数据并返回
    for item in items.items():
        yield {
            'image':item.find("img[width='90']").attr.src,
            'title':item.find("div.pl2 a").text().replace(' ','').replace("\n",''),
            'author':item.find("p.pl").text(),
            'score':item.find("div.star.clearfix span.rating_nums").text(),
        } 

def writeFile(content):
	with open("./result.txt",'a',encoding='utf-8') as f:
		f.write(json.dumps(content,ensure_ascii=False) + "\n")

def main(offset):
    ''' 主程序函数，负责调度执行爬虫处理 '''
    url = 'https://book.douban.com/top250?start=' + str(offset)
    #print(url)
    html = getPage(url)
    #判断是否爬取到数据，并调用解析函数
    if html:
        for item in parsePage(html):
            writeFile(item)

# 判断当前执行是否为主程序运行，并遍历调用主函数爬取数据
if __name__ == '__main__':
    for i in range(10):
        main(i)

	